import commonStyle from './../Style/commonStyle'


const translateProps =(props)=>{
    let _style={...commonStyle.default}
    if(props.disable){
        _style={..._style,...commonStyle.disable}
    }

    const newProps ={...props , styles:_style}
    return newProps;
}

export default (WrappedComponent)=>{
    return function wrappedRender(args){
        return WrappedComponent(translateProps(args))
    }
}