import React from 'react';
import ButtonOne from './Componets/ButtonOne'
import ButtonTwo from './Componets/ButtonTwo'

function App() {
  return (
    <div className="App">
       <ButtonOne disable/>
       <ButtonTwo disable/>
    </div>
  );
}

export default App;
