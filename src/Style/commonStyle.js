const styles = {
    default: {
        backgroundColor: 'red',
        color: 'white',
        padding: '10px'
    },
    disable:{
        backgroundColor: 'gray',
        color: 'orange'        
    }
}

export default styles